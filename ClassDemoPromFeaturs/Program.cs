﻿using System;
using System.Collections.Generic;

namespace ClassDemoPromFeaturs
{
    internal class Program
    {
        static void Main(string[] args)
        {
            List<int> numbers = new List<int>() { 5, 8, -9, -2, 0, 7, 6, 12, -42 };

            List<int> positiveNums = FilterNumbers(numbers, num => num >= 0 );

            CheckNumber myLambdaCheck = num => { return num >= 0; };
            ExtraParams myExtraParams = (num, message, check) => 
            {
                // something with num

                // something with message

                // something with check

                return true;
            };

            foreach (int num in positiveNums)
            {
                Console.WriteLine(num);
            }
        }

        public delegate bool CheckNumber(int num);
        public delegate bool ExtraParams(int num, string message, bool check);

        public static bool IsEven(int num)
        {
            return num % 2 == 0;
        }
        public static bool IsOdd(int num)
        {
            return !IsEven(num);
        }
        public static bool IsPositive(int num)
        {
            return num >= 0;
        }
        public static bool IsNegative(int num)
        {
            return num < 0;
        }

        // Func -> has any return type except boolean
        // Action -> has a void return type
        // Predicate -> has a boolean return type

        public static List<int> FilterNumbers(List<int> numbers, CheckNumber check)
        {
            List<int> outputNumbers = new List<int>();

            foreach (int i in numbers)
            {
                if (check(i))
                    outputNumbers.Add(i);
            }

            return outputNumbers;
        }
    }
}
