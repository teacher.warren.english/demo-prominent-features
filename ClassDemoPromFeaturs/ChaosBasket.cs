﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassDemoPromFeaturs
{
    public class ChaosBasket<TYPE, U>
    {
        TYPE[] BasketCollection = new TYPE[10];
        U[] Basket2 = new U[10];

        public void AddToBasket1(TYPE item)
        {
            Random random = new Random();
            BasketCollection[random.Next(BasketCollection.Length)] = item;
        }

        public void AddToBasket2(U item)
        {
            Random r = new Random();
            Basket2[r.Next(Basket2.Length)] = item;
        }

        public void DisplayContents()
        {
            Console.WriteLine("Basket 1:");
            foreach(TYPE item in BasketCollection)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("\nBasket 2:");

            foreach(U item in Basket2)
            {
                Console.WriteLine(item);
            }
        }
    }
}
